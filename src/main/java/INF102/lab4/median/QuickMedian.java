package INF102.lab4.median;

import java.util.ArrayList;
import java.util.List;

public class QuickMedian implements IMedian {

    @Override
    public <T extends Comparable<T>> T median(List<T> list) { // O(n)
        List<T> listCopy = new ArrayList<>(list); // Create a copy to avoid modifying the original list

        int n = listCopy.size(); // O(1)
        int k = n / 2; // Index of the median element //O(1)

        int left = 0; // O(1)
        int right = n - 1; // O(1)

        while (left < right) { // O(n)
            int pivotIndex = partition(listCopy, left, right); // O(n)

            if (pivotIndex == k) { // O(1)
                return listCopy.get(pivotIndex); // O(1)
            } else if (pivotIndex < k) { // O(1)
                left = pivotIndex + 1; // O(1)
            } else { // O(1)
                right = pivotIndex - 1; // O(1)
            }
        }

        return listCopy.get(left); // If left == right, it's the median //O(1)
    }

    private <T extends Comparable<T>> int partition(List<T> list, int left, int right) { // O(n)
        T pivot = list.get(right); // O(1)
        int i = left - 1; // index of smaller element //O(1)

        for (int j = left; j < right; j++) { // O(n)
            if (list.get(j).compareTo(pivot) <= 0) { // O(1)
                i++; // O(1)
                swap(list, i, j); // O(1)
            }
        }

        swap(list, i + 1, right); // O(1)
        return i + 1; // O(1)
    }

    private <T extends Comparable<T>> void swap(List<T> list, int i, int j) { // O(1)
        T temp = list.get(i); // O(1)
        list.set(i, list.get(j)); // O(1)
        list.set(j, temp); // O(1)
    }
}
