package INF102.lab4.sorting;

import java.util.List;

public class BubbleSort implements ISort {

    @Override
    public <T extends Comparable<T>> void sort(List<T> list) {// O(n^2)

        int n = list.size(); // O(1)
        for (int i = 0; i < n - 1; i++) { // O(n)
            for (int j = 0; j < n - i - 1; j++) {// O(n)
                if (list.get(j).compareTo(list.get(j + 1)) > 0) { // O(1)
                    T temp = list.get(j); // O(1)
                    list.set(j, list.get(j + 1)); // O(1)
                    list.set(j + 1, temp); // O(1)
                }
            }
        }
    }
}
